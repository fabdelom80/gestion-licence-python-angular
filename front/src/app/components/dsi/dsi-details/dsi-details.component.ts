import { Component, OnInit } from '@angular/core';
import {ApiService} from '../../../services/api.service';
import {ActivatedRoute, Router} from '@angular/router';
import {Logiciel} from '../../../models/logiciel.model';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Dsi} from '../../../models/dsi.model';

@Component({
  selector: 'app-dsi-details',
  templateUrl: './dsi-details.component.html',
  styleUrls: ['./dsi-details.component.css']
})
export class DsiDetailsComponent implements OnInit {

  private currentDsi = new Dsi(0, 'a');
  private message = '';
  private form: FormGroup;
  private success: boolean;

  constructor(
    private apiService: ApiService,
    private route: ActivatedRoute,
    private fb: FormBuilder,
    private router: Router) { }

  ngOnInit(): void {
    this.initForm();
    this.apiService.setBaseUrl('dsi');
    if (this.route.snapshot.paramMap.get('id') !== 'nouveau') {
      this.getDsi(this.route.snapshot.paramMap.get('id'));
      this.message = 'Modifier';
    }
    else {
      this.message = 'Ajouter';
    }
  }


  getDsi(id): void {
    this.apiService.get(id)
      .subscribe(
        data => {
          this.currentDsi = new Dsi(data.id, data.nom);
          this.form = this.fb.group({ // créer une instance de formulaire (FormGroup)
            nom: [data.nom, Validators.required],
          });
        },
        error => {
          console.log(error);
          this.currentDsi = null;
        });
  }

  initForm(): void {
    this.form = this.fb.group({ // créer une instance de formulaire (FormGroup)
      nom: ['', Validators.required],
    });
  }


  submit(): void {
    if (this.route.snapshot.paramMap.get('id') !== 'nouveau') {
      this.updateDsi();
    }
    else{
      this.saveDsi();
    }
  }

  saveDsi(): void {
    this.apiService.create(this.form.value)
      .subscribe(
        response => {
          console.log(response);
          this.success = true;
        },
        error => {
          console.log(error);
          this.success = false;
        });
  }


  updateDsi(): void {
    this.apiService.update(this.currentDsi.getId(), this.form.value)
      .subscribe(
        response => {
          console.log(response);
          this.success = true;
        },
        error => {
          console.log(error);
          this.success = false;
        });
  }

  deleteDsi(): void {
    this.apiService.delete(this.currentDsi.getId())
      .subscribe(
        response => {
          console.log(response);
          this.success = true;
          this.router.navigate(['/dsis']);
        },
        error => {
          console.log(error);
          this.success = false;
        });
  }

  public getCurrentDsi(): Dsi {
    return this.currentDsi;
  }

  public getMessage(): string {
    return this.message;
  }

  public getForm(): FormGroup{
    return this.form;
  }

  public getSucces(): boolean {
    return this.success;
  }

}
