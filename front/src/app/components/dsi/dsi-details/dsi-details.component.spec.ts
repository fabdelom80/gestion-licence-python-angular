import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DsiDetailsComponent } from './dsi-details.component';

describe('DsiDetailsComponent', () => {
  let component: DsiDetailsComponent;
  let fixture: ComponentFixture<DsiDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DsiDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DsiDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
