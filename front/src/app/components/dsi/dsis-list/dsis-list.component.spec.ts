import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DsisListComponent } from './dsis-list.component';

describe('DsisListComponent', () => {
  let component: DsisListComponent;
  let fixture: ComponentFixture<DsisListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DsisListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DsisListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
