import {Component, OnInit, Output} from '@angular/core';
import {ApiService} from '../../../services/api.service';
import {Dsi} from '../../../models/dsi.model';

@Component({
  selector: 'app-dsis-list',
  templateUrl: './dsis-list.component.html',
  styleUrls: ['./dsis-list.component.css']
})
export class DsisListComponent implements OnInit {


  @Output() private dsis: Array<Dsi> = [];
  @Output() private colonnes: Array<any> = [];
  private nom = '';

  constructor(private apiService: ApiService) { }

  ngOnInit(): void {
    this.apiService.setBaseUrl('dsi');
    this.retrieveDsis();
    this.colonnes.push('nom');
  }

  retrieveDsis(): void {
    this.apiService.getAll()
      .subscribe(
        data => {
          for (const i in data) {
            const dsi = new Dsi(data[i].id, data[i].nom);
            this.dsis.push(dsi);
          }
        },
        error => {
          console.log(error);
        });
  }


  searchNom(): void {
    this.apiService.findByNom(this.nom)
      .subscribe(
        data => {
          this.dsis = data;
          console.log(data);
        },
        error => {
          console.log(error);
        });
  }

  public getDsis(): Array<Dsi> {
    return this.dsis;
  }

  public getColonnes(): Array<any> {
    return this.colonnes;
  }

}
