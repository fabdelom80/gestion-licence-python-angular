import {Component, Input, OnInit} from '@angular/core';
import {Editeur} from '../../models/editeur.model';

@Component({
  selector: 'app-tab',
  templateUrl: './tab.component.html',
  styleUrls: ['./tab.component.css']
})
export class TabComponent implements OnInit {

  @Input() private colonnes: Array<any> = [];
  @Input() private objects: Array<any> = [];


  constructor() { }

  ngOnInit(): void {
  }

  public getObjects(): Array<Editeur> {
    return this.objects;
  }

  public getColonnes(): Array<any> {
    return this.colonnes;
  }

}
