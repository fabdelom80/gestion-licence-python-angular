import { Component, OnInit } from '@angular/core';
import {ApiService} from '../../../services/api.service';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-utilisateur-details',
  templateUrl: './utilisateur-details.component.html',
  styleUrls: ['./utilisateur-details.component.css']
})
export class UtilisateurDetailsComponent implements OnInit {

  currentUtilisateur = null;
  message = '';

  constructor(
    private apiService: ApiService,
    private route: ActivatedRoute,
    private router: Router) { }

  ngOnInit(): void {
    this.apiService.setBaseUrl('utilisateurs');
    this.message = '';
    this.getUtilisateur(this.route.snapshot.paramMap.get('id'));
  }

  getUtilisateur(id): void {
    this.apiService.get(id)
      .subscribe(
        data => {
          this.currentUtilisateur = data;
          console.log(data);
        },
        error => {
          console.log(error);
        });
  }


  updateUtilisateur(): void {
    this.apiService.update(this.currentUtilisateur.id, this.currentUtilisateur)
      .subscribe(
        response => {
          console.log(response);
          this.message = 'The utilisateur was updated successfully!';
        },
        error => {
          console.log(error);
        });
  }

  deleteUtilisateur(): void {
    this.apiService.delete(this.currentUtilisateur.id)
      .subscribe(
        response => {
          console.log(response);
          this.router.navigate(['/utilisateurs']);
        },
        error => {
          console.log(error);
        });
  }

}
