import { Component, OnInit } from '@angular/core';
import {ApiService} from '../../../services/api.service';

@Component({
  selector: 'app-utilisateurs-list',
  templateUrl: './utilisateurs-list.component.html',
  styleUrls: ['./utilisateurs-list.component.css']
})
export class UtilisateursListComponent implements OnInit {


  utilisateur: any;
  currentUtilisateur = null;
  currentIndex = -1;
  nom = '';

  constructor(private apiService: ApiService) { }

  ngOnInit(): void {
    this.apiService.setBaseUrl('utilisateurs');
    this.retrieveUtilisateurs();
  }

  retrieveUtilisateurs(): void {
    this.apiService.getAll()
      .subscribe(
        data => {
          this.utilisateur = data;
          console.log(data);
        },
        error => {
          console.log(error);
        });
  }

  refreshList(): void {
    this.retrieveUtilisateurs();
    this.currentUtilisateur = null;
    this.currentIndex = -1;
  }

  setActiveUtilisateur(editeur, index): void {
    this.currentUtilisateur = editeur;
    this.currentIndex = index;
  }

  removeAllUtilisateurs(): void {
    this.apiService.deleteAll()
      .subscribe(
        response => {
          console.log(response);
          this.retrieveUtilisateurs();
        },
        error => {
          console.log(error);
        });
  }

  searchNom(): void {
    this.apiService.findByNom(this.nom)
      .subscribe(
        data => {
          this.utilisateur = data;
          console.log(data);
        },
        error => {
          console.log(error);
        });
  }

}
