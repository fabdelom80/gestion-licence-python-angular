import { Component, OnInit } from '@angular/core';
import {ApiService} from '../../../services/api.service';
import {ActivatedRoute, Router} from '@angular/router';
import {Logiciel} from '../../../models/logiciel.model';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Metrique} from '../../../models/metrique.model';

@Component({
  selector: 'app-metrique-details',
  templateUrl: './metrique-details.component.html',
  styleUrls: ['./metrique-details.component.css']
})
export class MetriqueDetailsComponent implements OnInit {

  private currentMetrique = new Metrique(0, 'a');
  private message = '';
  private form: FormGroup;
  private success: boolean;

  constructor(
    private apiService: ApiService,
    private route: ActivatedRoute,
    private fb: FormBuilder,
    private router: Router) { }

  ngOnInit(): void {
    this.initForm();
    this.apiService.setBaseUrl('metriques');
    if (this.route.snapshot.paramMap.get('id') !== 'nouveau') {
      this.getMetrique(this.route.snapshot.paramMap.get('id'));
      this.message = 'Modifier';
    }
    else {
      this.message = 'Ajouter';
    }
  }


  getMetrique(id): void {
    this.apiService.get(id)
      .subscribe(
        data => {
          this.currentMetrique = new Metrique(data.id, data.type);
          this.form = this.fb.group({ // créer une instance de formulaire (FormGroup)
            type: [data.type, Validators.required],
          });
        },
        error => {
          console.log(error);
          this.currentMetrique = null;
        });
  }

  initForm(): void {
    this.form = this.fb.group({ // créer une instance de formulaire (FormGroup)
      type: ['', Validators.required],
    });
  }


  submit(): void {
    if (this.route.snapshot.paramMap.get('id') !== 'nouveau') {
      this.updateMetrique();
    }
    else{
      this.saveMetrique();
    }
  }

  saveMetrique(): void {
    this.apiService.create(this.form.value)
      .subscribe(
        response => {
          console.log(response);
          this.success = true;
        },
        error => {
          console.log(error);
          this.success = false;
        });
  }


  updateMetrique(): void {
    this.apiService.update(this.currentMetrique.getId(), this.form.value)
      .subscribe(
        response => {
          console.log(response);
          this.success = true;
        },
        error => {
          console.log(error);
          this.success = false;
        });
  }

  deleteMetrique(): void {
    this.apiService.delete(this.currentMetrique.getId())
      .subscribe(
        response => {
          console.log(response);
          this.success = true;
          this.router.navigate(['/metriques']);
        },
        error => {
          console.log(error);
          this.success = false;
        });
  }

  public getCurrentLogiciel(): Metrique {
    return this.currentMetrique;
  }

  public getMessage(): string {
    return this.message;
  }

  public getForm(): FormGroup{
    return this.form;
  }

  public getSucces(): boolean {
    return this.success;
  }

}
