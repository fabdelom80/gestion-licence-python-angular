import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MetriqueDetailsComponent } from './metrique-details.component';

describe('MetriqueDetailsComponent', () => {
  let component: MetriqueDetailsComponent;
  let fixture: ComponentFixture<MetriqueDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MetriqueDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MetriqueDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
