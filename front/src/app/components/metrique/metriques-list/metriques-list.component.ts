import {Component, OnInit, Output} from '@angular/core';
import {ApiService} from '../../../services/api.service';
import {Metrique} from '../../../models/metrique.model';

@Component({
  selector: 'app-metriques-list',
  templateUrl: './metriques-list.component.html',
  styleUrls: ['./metriques-list.component.css']
})
export class MetriquesListComponent implements OnInit {

  @Output() private metriques: Array<Metrique> = [];
  @Output() private colonnes: Array<any> = [];
  private nom = '';

  constructor(private apiService: ApiService) { }

  ngOnInit(): void {
    this.apiService.setBaseUrl('metriques');
    this.retrieveMetriques();
    this.colonnes.push('Type');
  }

  retrieveMetriques(): void {
    this.apiService.getAll()
      .subscribe(
        data => {
          for (const i in data) {
            const metrique = new Metrique(data[i].id, data[i].type);
            this.metriques.push(metrique);
          }
        },
        error => {
          console.log(error);
        });
  }


  searchNom(): void {
    this.apiService.findByNom(this.nom)
      .subscribe(
        data => {
          this.metriques = data;
          console.log(data);
        },
        error => {
          console.log(error);
        });
  }

  public getMetriques(): Array<Metrique> {
    return this.metriques;
  }

  public getColonnes(): Array<any> {
    return this.colonnes;
  }


}
