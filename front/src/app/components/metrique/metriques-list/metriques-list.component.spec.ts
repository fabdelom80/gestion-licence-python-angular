import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MetriquesListComponent } from './metriques-list.component';

describe('MetriquesListComponent', () => {
  let component: MetriquesListComponent;
  let fixture: ComponentFixture<MetriquesListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MetriquesListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MetriquesListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
