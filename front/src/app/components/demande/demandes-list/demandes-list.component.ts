import {Component, OnInit, Output} from '@angular/core';
import {Logiciel} from '../../../models/logiciel.model';
import {ApiService} from '../../../services/api.service';
import {Demande} from '../../../models/demande.model';

@Component({
  selector: 'app-demandes-list',
  templateUrl: './demandes-list.component.html',
  styleUrls: ['./demandes-list.component.css']
})
export class DemandesListComponent implements OnInit {


  @Output() private demandes: Array<Demande> = [];
  @Output() private colonnes: Array<any> = [];
  private nom = '';

  constructor(private apiService: ApiService) { }

  ngOnInit(): void {
    this.apiService.setBaseUrl('demandes');
    this.retrieveDemandes();
    this.colonnes.push('Utilisateur');
    this.colonnes.push('Projet');
    this.colonnes.push('DSI');
    this.colonnes.push('Date souhaitée');
    this.colonnes.push('Date clôture');
  }

  retrieveDemandes(): void {
    this.apiService.setBaseUrl('demandes');
    this.apiService.getAll()
      .subscribe(
        data => {
          for (const i in data) {
            // tslint:disable-next-line:max-line-length
            const demande = new Demande(data[i].id, data[i].statut, data[i].dateCreation, data[i].dateCloture, data[i].dateSouhaitee, data[i].projet, data[i].clientDDSI, data[i].mailProposition, data[i].droitUsageLogiciel, data[i].type, data[i].quantite, data[i].coreFactor, data[i].information, data[i].utilisateur.nom + data[i].utilisateur.prenom, data[i].dsi.nom);
            this.demandes.push(demande);
            this.getLogicielsByDemande(demande);
            this.getServeursByDemande(demande);
          }
          console.log(data);
        },
        error => {
          console.log(error);
        });
  }

  searchNom(): void {
    this.apiService.findByNom(this.nom)
      .subscribe(
        data => {
          this.demandes = data;
          console.log(data);
        },
        error => {
          console.log(error);
        });
  }

  public getDemandes(): Array<Demande> {
    return this.demandes;
  }

  public getColonnes(): Array<any> {
    return this.colonnes;
  }

  public getLogicielsByDemande(demande): void {
    this.apiService.setBaseUrl('logicielDemande/demande/' + demande.getId());
    this.apiService.getAll()
      .subscribe(
        data => {
          demande.addLogiciels(data);
        },
        error => {
          console.log(error);
        });
  }

  public getServeursByDemande(demande): void {
    this.apiService.setBaseUrl('serveurs/demande/' + demande.getId());
    this.apiService.getAll()
      .subscribe(
        data => {
          demande.addServeurs(data);
        },
        error => {
          console.log(error);
        });
  }

}
