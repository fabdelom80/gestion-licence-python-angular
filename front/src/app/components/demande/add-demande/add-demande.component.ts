import {Component, Inject, OnInit, Output, ViewChild} from '@angular/core';
import {ApiService} from '../../../services/api.service';
import {FormGroup, Validators, FormBuilder} from '@angular/forms';
import {DatePipe, DOCUMENT} from '@angular/common';
import {Demande} from '../../../models/demande.model';


@Component({
  selector: 'app-add-demande',
  templateUrl: './add-demande.component.html',
  styleUrls: ['./add-demande.component.css']
})
export class AddDemandeComponent implements OnInit {

  @Output() private step = 1;
  private userForm: FormGroup;
  private projetForm: FormGroup;
  @Output() private logicielForm: FormGroup;
  private metriqueUserForm: FormGroup;
  public metriqueServerForm1: FormGroup;
  private _type = 1;
  private editeurSelect = 1;
  private editeurs;
  private dsis;
  private logiciels: Array<any> = [];
  private _metriqueUtilisateur = 0;
  private _metriqueServeur = 1;
  private dateDujour;
  private demande;
  public listFormServer: Array<any> = [];

  constructor(private apiService: ApiService,
              private fb: FormBuilder,
              @Inject(DOCUMENT) document,
              private datepipe: DatePipe) { }

  ngOnInit(): void {
    this.initDate();
    this.initForms();
    this.initGet();
  }


  public nextStep(): void{
    if(this.step === 3 && this.getMetrique(1) < 1){
      this.step++;
    } // en fonction des métriques, on affiche les formulaires
    if(this.step === 4 && this.getMetrique(2) < 1){
      this.step++;
    }
    this.step++;
  }



  addOne(i: number): number {
    return i+1;
  }

  saveDemande(): void {
    this.apiService.setBaseUrl('demandes');
    this.apiService.create(this.demande)
      .subscribe(
        response => {
          this.newLogicielDemande(response.id);
          this.newServeur(response.id)
        },
        error => {
          console.log(error);
        });
  }


  newDemande(): void {
    const dateParts = this.userForm.controls['dateRea'].value.split('/');
    this.demande = new Demande(0, 1, this.datepipe.transform(this.dateDujour, 'yyyy-MM-dd'), null, this.datepipe.transform(new Date(dateParts[2] + '-' + (dateParts[1]) + '-' + dateParts[0]), 'yyyy-MM-dd'), this.projetForm.controls['nomProjet'].value, this.projetForm.controls['clientDDSI'].value, this.userForm.controls['mailProposition'].value, 'rien',this._type, 12,12,"Pas d'info",1,1);
    this.saveDemande();

  }

  //Pour ajouter les logiciels selectionné dans la table logicielDemande
  private newLogicielDemande(idDemande): void {
    let logicielDemande: Array<any> = [];
    console.log(this.logiciels);
    for (const i in this.logiciels) {
      const data = {
        logiciel: this.logiciels[i].id,
        demande: idDemande,
      };
      logicielDemande.push(data);
    }
    this.apiService.setBaseUrl('logicielDemande');
    this.apiService.create(logicielDemande)
      .subscribe(
        response => {
          console.log(response);
        },
        error => {
          console.log(error);
        });

  }

  //Pour ajouter les serveurs selectionnés dans la table serveurs
  private newServeur(idDemande): void {
    const serveurs: Array<any> = [];
    for (const i in this.listFormServer) {
      const data = {
        nom: this.listFormServer[i].controls['nomServeur'].value,
        physique: this.listFormServer[i].controls['physique'].value,
        typeCpu: 'Intel',
        modele: this.listFormServer[i].controls['modele'].value,
        cpu : this.listFormServer[i].controls['nbrCpu'].value,
        cores: this.listFormServer[i].controls['nbrCore'].value,
        demande: idDemande
      };
      serveurs.push(data);
    }
    this.apiService.setBaseUrl('serveurs');
    this.apiService.create(serveurs)
      .subscribe(
        response => {
          console.log(response);
        },
        error => {
          console.log(error);
        });

  }



  // ******* INIT *****
  private initDate(): void {
    let ladate = new Date();
    this.dateDujour = new Date(ladate.getFullYear() + '-' + (ladate.getMonth() + 1) + '-' + ladate.getDate());
  }

  private initForms(): void {

    this.userForm = this.fb.group({ // créer une instance de formulaire (FormGroup)
      cp: ['9103891J', [Validators.required, Validators.minLength(7)]], //créer une instance de FormControl
      mail: ['fabien.brunet@sncf.fr', Validators.required],
      mailProposition: ['', Validators.required],
      nom: ['BRUNET', Validators.required],
      prenom: ['Fabien', Validators.required],
      dateRea: [this.dateDujour.toLocaleDateString("fr",{timeZone:"UTC"}), Validators.required],
      selectDSI: [1, Validators.required],
    });

    this.projetForm = this.fb.group({ // créer une instance de formulaire (FormGroup)
      nomProjet: ['', Validators.required],
      clientDDSI: ['', Validators.required],
      selectDSIProjet: [1, Validators.required],
    });

    this.logicielForm = this.fb.group({ // créer une instance de formulaire (FormGroup)
      selectEditeur: [3, Validators.required],
      selectLogiciel: [1, Validators.required],
    });
    this.metriqueServerForm1 = this.fb.group({ // créer une instance de formulaire (FormGroup)
      physique: [true],
      nomServeur: ['', Validators.required],
      modele: ['', Validators.required],
      nbrCpu: ['', Validators.required],
      nbrCore: ['', Validators.required],
    });
    this.listFormServer.push(this.metriqueServerForm1);
    this.metriqueUserForm = this.fb.group({ // créer une instance de formulaire (FormGroup)
      quantiteUtilisateur: ['', Validators.required],
    });
  }

  private modFormServer(event, index): void {
    if (event.target.checked) {
      const form = this.fb.group({ // créer une instance de formulaire (FormGroup)
        physique: [true],
        nomServeur: ['', Validators.required],
        modele: ['', Validators.required],
        nbrCpu: ['', Validators.required],
        nbrCore: ['', Validators.required],
      });
      this.listFormServer.push(form);
    }
    else {
      this.listFormServer.splice(index,1);
    }
  }

  // ************** init GET ************
  private initGet(): void  {
    this.initDsi();
    this.initEditeur();
    this.initLogiciel();
  }

  private initEditeur(): void {
    this.apiService.setBaseUrl('editeurs');
    this.apiService.getAll()
      .subscribe(
        data => {
          this.editeurs = data;
        },
        error => {
          console.log(error);
          this.editeurs = null;
        });
  }

  private initDsi(): void {
    this.apiService.setBaseUrl('dsi');

    this.apiService.getAll()
      .subscribe(
        data => {
          this.dsis = data;
        },
        error => {
          console.log(error);
          this.dsis = null;
        });
  }

  private initLogiciel(): void {
    this.editeurSelect = this.logicielForm.controls['selectEditeur'].value;
  }

  // ************** Fin init GET ************


  // ********** Stepper and getter ********

  public setStep(step): void{
    this.step = step;
  }

  public selectLogiciel(logiciel): void {
    this.logiciels.push(logiciel);
  }

  public deselectLogiciel(logiciel): void {
    this.logiciels = this.logiciels.filter(log => log.id !== parseInt(logiciel.id));
  }

  public resetSelectLogiciel(): void {
    this.logiciels = [];
  }

  public getMetrique(number: number): number {
    return this.logiciels.filter(log => log.metrique !== number).length;
  }

  get type(): number {
    return this._type;
  }

  public setType(value: number): void {
    this._type = value;
  }
  set metriqueUtilisateur(value: number) {
    this._metriqueUtilisateur = value;
  }
  get metriqueServeur(): number {
    return this._metriqueServeur;
  }

  set metriqueServeur(value: number) {
    this._metriqueServeur = value;
  }
  get metriqueUtilisateur(): number {
    return this._metriqueUtilisateur;
  }

  public setPhysiqueForm(value: boolean, index): void {
    this.listFormServer[index].controls['physique'].setValue(value);
  }
  public getUserForm(): FormGroup {
    return this.userForm;
  }
  public getProjetForm(): FormGroup {
    return this.projetForm;
  }

  public setEditeurSelect(value: number): void {
    this.editeurSelect = value;
  }
  @Output() public getEditeurSelect(): number {
    return this.editeurSelect;
  }

  public getStep(): number {
    return this.step;
  }

  // ****** fin stepper and getter *******


}
