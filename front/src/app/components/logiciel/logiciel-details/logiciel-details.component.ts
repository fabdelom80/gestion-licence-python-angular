import {Component, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import {ApiService} from '../../../services/api.service';
import {ActivatedRoute, Router} from '@angular/router';
import {FormBuilder, FormControl, FormGroup, ReactiveFormsModule, Validators} from '@angular/forms';
import {Metrique} from '../../../models/metrique.model';
import {Editeur} from '../../../models/editeur.model';
import {Logiciel} from '../../../models/logiciel.model';
import {EditeurService} from '../../../services/editeur.service';
import {Observable} from 'rxjs';

@Component({
  selector: 'app-logiciel-details',
  templateUrl: './logiciel-details.component.html',
  styleUrls: ['./logiciel-details.component.css']
})
export class LogicielDetailsComponent implements OnInit {

  private currentLogiciel = new Logiciel(0, 'a',0,0,'a','a',0,0);
  private message = '';
  private form: FormGroup;
  private success: boolean;
  private editeurs: Array<Editeur> = [];
  private metriques: Array<Metrique> = [];

  constructor(
    private apiService: ApiService,
    private route: ActivatedRoute,
    private fb: FormBuilder,
    private router: Router) { }

  ngOnInit(): void {
    this.initForm();
    this.getEditeur();
    this.getMetrique();
    if (this.route.snapshot.paramMap.get('id') !== 'nouveau') {
      this.getLogiciel(this.route.snapshot.paramMap.get('id'));
      this.message = 'Modifier';
    }
    else {
      this.message = 'Ajouter';
    }
  }


  getLogiciel(id): void {
    this.apiService.setBaseUrl('logiciels');
    this.apiService.get(id)
      .subscribe(
        data => {
          this.currentLogiciel = new Logiciel(data.id, data.nom, data.nbLicenceTotal, data.nbLicenceRestante, data.descriptif, data.uniteDeMesure, data.metrique, data.editeur);
          this.form = this.fb.group({ // créer une instance de formulaire (FormGroup)
            nom: [data.nom, Validators.required],
            nbLicenceTotal: [data.nbLicenceTotal, Validators.required],
            nbLicenceRestante: [data.nbLicenceRestante, Validators.required],
            descriptif: [data.descriptif, Validators.required],
            uniteDeMesure: [data.uniteDeMesure, Validators.required],
            metrique: [data.metrique.id, Validators.required],
            editeur: [data.editeur.id, Validators.required],
          });
        },
        error => {
          console.log(error);
          this.currentLogiciel = null;
          this.success = false;
        });
  }

  private getEditeur(): void {
    this.apiService.setBaseUrl('editeurs');
    this.apiService.getAll()
      .subscribe(
        data => {
          for (const i in data) {
            this.editeurs.push(new Editeur(data[i].id, data[i].nom));
          }
          this.form.controls['editeur'].setValue(data[0].id);
        },
        error => {
          console.log(error);
          this.editeurs = null;
        });
  }

  private getMetrique(): void {
    this.apiService.setBaseUrl('metriques');
    this.apiService.getAll()
      .subscribe(
        data => {
          for (const i in data) {
            this.metriques.push(new Metrique(data[i].id, data[i].type));
          }
          this.form.controls['metrique'].setValue(data[0].id);
          console.log(this.metriques);
        },
        error => {
          console.log(error);
          this.metriques = null;
        });
  }

  initForm(): void {
    this.form = this.fb.group({ // créer une instance de formulaire (FormGroup)
      nom: ['', Validators.required],
      nbLicenceTotal: ['', Validators.required],
      nbLicenceRestante: ['', Validators.required],
      descriptif: ['', Validators.required],
      uniteDeMesure: ['', Validators.required],
      metrique: ['', Validators.required],
      editeur: ['', Validators.required],
    });
  }


  submit(): void {
    if (this.route.snapshot.paramMap.get('id') !== 'nouveau') {
      this.updateLogiciel();
    }
    else{
      this.saveLogiciel();
    }
  }

  saveLogiciel(): void {
    this.apiService.setBaseUrl('logiciels');
    this.apiService.create(this.form.value)
      .subscribe(
        response => {
          console.log(response);
          this.success = true;
        },
        error => {
          console.log(error);
          this.success = false;
        });
  }


  updateLogiciel(): void {
    this.apiService.update(this.currentLogiciel.getId(), this.form.value)
      .subscribe(
        response => {
          console.log(response);
          this.success = true;
        },
        error => {
          console.log(error);
          this.success = false;
        });
  }

  deleteLogiciel(): void {
    this.apiService.delete(this.currentLogiciel.getId())
      .subscribe(
        response => {
          console.log(response);
          this.success = true;
          this.router.navigate(['/logiciels']);
        },
        error => {
          console.log(error);
          this.success = false;
        });
  }

  public getCurrentLogiciel(): Logiciel {
    return this.currentLogiciel;
  }

  public getMessage(): string {
    return this.message;
  }

  public getForm(): FormGroup{
    return this.form;
  }

  public getSucces(): boolean {
    return this.success;
  }


  public setEditeur(id): void {
    this.form.controls['editeur'].setValue(id);
  }

  public setMetrique(id): void {
    this.form.controls['metrique'].setValue(id);
  }

}
