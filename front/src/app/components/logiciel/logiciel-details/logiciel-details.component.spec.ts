import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LogicielDetailsComponent } from './logiciel-details.component';

describe('LogicielDetailsComponent', () => {
  let component: LogicielDetailsComponent;
  let fixture: ComponentFixture<LogicielDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LogicielDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LogicielDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
