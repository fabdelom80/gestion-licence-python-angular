import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LogicielsListComponent } from './logiciels-list.component';

describe('LogicielsListComponent', () => {
  let component: LogicielsListComponent;
  let fixture: ComponentFixture<LogicielsListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LogicielsListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LogicielsListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
