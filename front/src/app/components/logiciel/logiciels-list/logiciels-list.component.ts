import {Component, OnInit, Output} from '@angular/core';
import {ApiService} from '../../../services/api.service';
import {Logiciel} from '../../../models/logiciel.model';
import {Editeur} from '../../../models/editeur.model';

@Component({
  selector: 'app-logiciels-list',
  templateUrl: './logiciels-list.component.html',
  styleUrls: ['./logiciels-list.component.css']
})
export class LogicielsListComponent implements OnInit {


  @Output() private logiciels: Array<Logiciel> = [];
  @Output() private colonnes: Array<any> = [];
  private nom = '';

  constructor(private apiService: ApiService) { }

  ngOnInit(): void {
    this.apiService.setBaseUrl('logiciels');
    this.retrieveLogiciels();
    this.colonnes.push('Nom');
    this.colonnes.push('Editeur');
    this.colonnes.push('Métrique');
    this.colonnes.push('Nombre de licence totale');
    this.colonnes.push('Nombre de licence restante');
  }

  retrieveLogiciels(): void {
    this.apiService.getAll()
      .subscribe(
        data => {
          for (const i in data) {
            const logiciel = new Logiciel(data[i].id, data[i].nom, data[i].nbLicenceTotal, data[i].nbLicenceRestante, data[i].descriptif, data[i].uniteDeMesure, data[i].metrique.type, data[i].editeur.nom);
            this.logiciels.push(logiciel);
          }
          console.log(this.logiciels);
        },
        error => {
          console.log(error);
        });
  }

  searchNom(): void {
    this.apiService.findByNom(this.nom)
      .subscribe(
        data => {
          this.logiciels = data;
          console.log(data);
        },
        error => {
          console.log(error);
        });
  }

  public getLogiciels(): Array<Logiciel> {
    return this.logiciels;
  }

  public getColonnes(): Array<any> {
    return this.colonnes;
  }

}
