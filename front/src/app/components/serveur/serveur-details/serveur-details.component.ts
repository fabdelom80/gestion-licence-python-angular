import { Component, OnInit } from '@angular/core';
import {ApiService} from '../../../services/api.service';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-serveur-details',
  templateUrl: './serveur-details.component.html',
  styleUrls: ['./serveur-details.component.css']
})
export class ServeurDetailsComponent implements OnInit {

  currentServeur = null;
  message = '';

  constructor(
    private apiService: ApiService,
    private route: ActivatedRoute,
    private router: Router) { }

  ngOnInit(): void {
    this.apiService.setBaseUrl('serveurs');
    this.message = '';
    this.getServeur(this.route.snapshot.paramMap.get('id'));
  }

  getServeur(id): void {
    this.apiService.get(id)
      .subscribe(
        data => {
          this.currentServeur = data;
          console.log(data);
        },
        error => {
          console.log(error);
        });
  }


  updateServeur(): void {
    this.apiService.update(this.currentServeur.id, this.currentServeur)
      .subscribe(
        response => {
          console.log(response);
          this.message = 'The serveur was updated successfully!';
        },
        error => {
          console.log(error);
        });
  }

  deleteServeur(): void {
    this.apiService.delete(this.currentServeur.id)
      .subscribe(
        response => {
          console.log(response);
          this.router.navigate(['/serveurs']);
        },
        error => {
          console.log(error);
        });
  }

}
