import { Component, OnInit } from '@angular/core';
import {ApiService} from '../../../services/api.service';

@Component({
  selector: 'app-serveurs-list',
  templateUrl: './serveurs-list.component.html',
  styleUrls: ['./serveurs-list.component.css']
})
export class ServeursListComponent implements OnInit {


  serveurs: any;
  currentServeur = null;
  currentIndex = -1;
  nom = '';

  constructor(private apiService: ApiService) { }

  ngOnInit(): void {
    this.apiService.setBaseUrl('serveurs');
    this.retrieveServeurs();
  }

  retrieveServeurs(): void {
    this.apiService.getAll()
      .subscribe(
        data => {
          this.serveurs = data;
          console.log(data);
        },
        error => {
          console.log(error);
        });
  }

  refreshList(): void {
    this.retrieveServeurs();
    this.currentServeur = null;
    this.currentIndex = -1;
  }

  setActiveServeur(serveur, index): void {
    this.currentServeur = serveur;
    this.currentIndex = index;
  }

  removeAllServeurs(): void {
    this.apiService.deleteAll()
      .subscribe(
        response => {
          console.log(response);
          this.retrieveServeurs();
        },
        error => {
          console.log(error);
        });
  }

  searchNom(): void {
    this.apiService.findByNom(this.nom)
      .subscribe(
        data => {
          this.serveurs = data;
          console.log(data);
        },
        error => {
          console.log(error);
        });
  }

}
