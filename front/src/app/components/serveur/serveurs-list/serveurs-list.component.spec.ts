import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ServeursListComponent } from './serveurs-list.component';

describe('ServeursListComponent', () => {
  let component: ServeursListComponent;
  let fixture: ComponentFixture<ServeursListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ServeursListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ServeursListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
