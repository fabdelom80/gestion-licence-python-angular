import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-stepper',
  templateUrl: './stepper.component.html',
  styleUrls: ['./stepper.component.css']
})
export class StepperComponent implements OnInit {

  @Input() private step;
  @Input() private user;
  @Input() private projet;
  @Output() public changeStep = new EventEmitter<any>();
  constructor() { }

  ngOnInit(): void {
  }

  public getStep(): number {
    return this.step;
  }
  public getProjet(): string {
    return this.projet;
  }

  public getUser(): string {
    return this.user;
  }


}
