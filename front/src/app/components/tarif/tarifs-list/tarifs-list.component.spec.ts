import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TarifsListComponent } from './tarifs-list.component';

describe('TarifsListComponent', () => {
  let component: TarifsListComponent;
  let fixture: ComponentFixture<TarifsListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TarifsListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TarifsListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
