import { Component, OnInit } from '@angular/core';
import {ApiService} from '../../../services/api.service';

@Component({
  selector: 'app-tarifs-list',
  templateUrl: './tarifs-list.component.html',
  styleUrls: ['./tarifs-list.component.css']
})
export class TarifsListComponent implements OnInit {


  tarifs: any;
  currentTarif = null;
  currentIndex = -1;
  nom = '';

  constructor(private apiService: ApiService) { }

  ngOnInit(): void {
    this.apiService.setBaseUrl('tarifs');
    this.retrieveTarifs();
  }

  retrieveTarifs(): void {
    this.apiService.getAll()
      .subscribe(
        data => {
          this.tarifs = data;
          console.log(data);
        },
        error => {
          console.log(error);
        });
  }

  refreshList(): void {
    this.retrieveTarifs();
    this.currentTarif = null;
    this.currentIndex = -1;
  }

  setActiveTarif(tarif, index): void {
    this.currentTarif = tarif;
    this.currentIndex = index;
  }

  removeAllTarifs(): void {
    this.apiService.deleteAll()
      .subscribe(
        response => {
          console.log(response);
          this.retrieveTarifs();
        },
        error => {
          console.log(error);
        });
  }

  searchNom(): void {
    this.apiService.findByNom(this.nom)
      .subscribe(
        data => {
          this.tarifs = data;
          console.log(data);
        },
        error => {
          console.log(error);
        });
  }
}
