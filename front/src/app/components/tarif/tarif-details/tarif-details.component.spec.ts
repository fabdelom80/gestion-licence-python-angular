import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TarifDetailsComponent } from './tarif-details.component';

describe('TarifDetailsComponent', () => {
  let component: TarifDetailsComponent;
  let fixture: ComponentFixture<TarifDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TarifDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TarifDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
