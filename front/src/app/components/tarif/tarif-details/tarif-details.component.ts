import { Component, OnInit } from '@angular/core';
import {ApiService} from '../../../services/api.service';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-tarif-details',
  templateUrl: './tarif-details.component.html',
  styleUrls: ['./tarif-details.component.css']
})
export class TarifDetailsComponent implements OnInit {

  currentTarif = null;
  message = '';

  constructor(
    private apiService: ApiService,
    private route: ActivatedRoute,
    private router: Router) { }

  ngOnInit(): void {
    this.apiService.setBaseUrl('tarifs');
    this.message = '';
    this.getTarif(this.route.snapshot.paramMap.get('id'));
  }

  getTarif(id): void {
    this.apiService.get(id)
      .subscribe(
        data => {
          this.currentTarif = data;
          console.log(data);
        },
        error => {
          console.log(error);
        });
  }


  updateTarif(): void {
    this.apiService.update(this.currentTarif.id, this.currentTarif)
      .subscribe(
        response => {
          console.log(response);
          this.message = 'The tarif was updated successfully!';
        },
        error => {
          console.log(error);
        });
  }

  deleteTarif(): void {
    this.apiService.delete(this.currentTarif.id)
      .subscribe(
        response => {
          console.log(response);
          this.router.navigate(['/tarifs']);
        },
        error => {
          console.log(error);
        });
  }

}
