import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListeDeroulanteMultiComponent } from './liste-deroulante-multi.component';

describe('ListeDeroulanteMultiComponent', () => {
  let component: ListeDeroulanteMultiComponent;
  let fixture: ComponentFixture<ListeDeroulanteMultiComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListeDeroulanteMultiComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListeDeroulanteMultiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
