import {Component, Input, OnChanges, OnInit, Output, SimpleChanges, EventEmitter} from '@angular/core';
import {ApiService} from '../../services/api.service';

@Component({
  selector: 'app-liste-deroulante-multi',
  templateUrl: './liste-deroulante-multi.component.html',
  styleUrls: ['./liste-deroulante-multi.component.css']
})
export class ListeDeroulanteMultiComponent implements OnInit, OnChanges {

  logiciels;
  @Input() editeurSelect: number;
  @Output() logicielSelect = new EventEmitter<any>();
  @Output() logicielDeselect = new EventEmitter<any>();
  @Output() resetLogicielSelect = new EventEmitter<any>();
  constructor(private apiService: ApiService) { }


  ngOnInit(): void {
  }


  ngOnChanges(changes: SimpleChanges): void {
    this.resetLogicielSelect.emit();
    this.initLogiciels();
  }


  private initLogiciels(): void {
    this.apiService.setBaseUrl('logiciels/editeur');
    this.apiService.get(this.editeurSelect)
      .subscribe(
        data => {
          this.logiciels = data;
        },
        error => {
          console.log(error);
          this.logiciels = null;
        });
  }

  test(logiciel, id): void {
    if (!this.logiciels[id]['active']) {
      this.logiciels[id]['active'] = true;
      this.logiciels[id]['selected'] = 'selected';
      this.logicielSelect.emit(logiciel);
    }else {
      this.logiciels[id]['active'] = false;
      this.logiciels[id]['selected'] = '';
      this.logicielDeselect.emit(logiciel);
    }
  }
}
