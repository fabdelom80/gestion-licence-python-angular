import {Component, OnChanges, OnInit} from '@angular/core';
import {ApiService} from '../../../services/api.service';
import { ActivatedRoute, Router } from '@angular/router';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {Editeur} from '../../../models/editeur.model';

@Component({
  selector: 'app-editeur-details',
  templateUrl: './editeur-details.component.html',
  styleUrls: ['./editeur-details.component.css']
})
export class EditeurDetailsComponent implements OnInit{

  private currentEditeur = new Editeur(0, 'a');
  private message = '';
  private form: FormGroup;
  private success: boolean;

  constructor(
    private apiService: ApiService,
    private route: ActivatedRoute,
    private fb: FormBuilder,
    private router: Router) { }

  ngOnInit(): void {
    this.initForm();
    this.apiService.setBaseUrl('editeurs');
    if (this.route.snapshot.paramMap.get('id') !== 'nouveau') {
      this.getEditeur(this.route.snapshot.paramMap.get('id'));
      this.message = 'Modifier';
    }
    else {
      this.message = 'Ajouter';
    }
  }


  getEditeur(id): void {
    this.apiService.get(id)
      .subscribe(
        data => {
          this.currentEditeur = new Editeur(data.id, data.nom);
          this.form = this.fb.group({ // créer une instance de formulaire (FormGroup)
            nom: [data.nom, Validators.required],
          });
        },
        error => {
          console.log(error);
          this.currentEditeur = null;
          this.success = false;
        });
  }

  initForm(): void {
    this.form = this.fb.group({ // créer une instance de formulaire (FormGroup)
      nom: ['', Validators.required],
    });
  }


  submit(): void {
    if (this.route.snapshot.paramMap.get('id') !== 'nouveau') {
      this.updateEditeur();
    }
    else{
      this.saveEditeur();
    }
  }

  saveEditeur(): void {
    this.apiService.create(this.form.value)
      .subscribe(
        response => {
          console.log(response);
          this.success = true;
        },
        error => {
          console.log(error);
          this.success = false;
        });
  }


  updateEditeur(): void {
    this.apiService.update(this.currentEditeur.getId(), this.form.value)
      .subscribe(
        response => {
          console.log(response);
          this.success = true;
        },
        error => {
          console.log(error);
          this.success = false;
        });
  }

  deleteEditeur(): void {
    this.apiService.delete(this.currentEditeur.getId())
      .subscribe(
        response => {
          console.log(response);
          this.success = true;
          this.router.navigate(['/editeurs']);
        },
        error => {
          console.log(error);
          this.success = false;
        });
  }

  public getCurrentEditeur(): Editeur {
    return this.currentEditeur;
  }

  public getMessage(): string {
    return this.message;
  }

  public getForm(): FormGroup{
    return this.form;
  }
  public getSucces(): boolean {
    return this.success;
  }


}
