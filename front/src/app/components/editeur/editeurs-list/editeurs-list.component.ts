import {Component, OnInit, Output} from '@angular/core';
import {ApiService} from '../../../services/api.service';
import {Editeur} from '../../../models/editeur.model';

@Component({
  selector: 'app-editeurs-list',
  templateUrl: './editeurs-list.component.html',
  styleUrls: ['./editeurs-list.component.css']
})
export class EditeursListComponent implements OnInit {

  @Output() private editeurs: Array<Editeur> = [];
  @Output() private colonnes: Array<any> = [];
  private nom = '';

  constructor(private apiService: ApiService) { }

  ngOnInit(): void {
    this.apiService.setBaseUrl('editeurs');
    this.retrieveEditeurs();
    this.colonnes.push('nom');
  }

  retrieveEditeurs(): void {
    this.apiService.getAll()
      .subscribe(
        data => {
          for (const i in data) {
            const editeur = new Editeur(data[i].id, data[i].nom);
            this.editeurs.push(editeur);
          }
        },
        error => {
          console.log(error);
        });
  }


  searchNom(): void {
    this.apiService.findByNom(this.nom)
      .subscribe(
        data => {
          this.editeurs = data;
          console.log(data);
        },
        error => {
          console.log(error);
        });
  }

  public getEditeurs(): Array<Editeur> {
    return this.editeurs;
  }

  public getColonnes(): Array<any> {
    return this.colonnes;
  }
}
