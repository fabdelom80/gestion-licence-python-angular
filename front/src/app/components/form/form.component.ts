import {Component, Input, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {ApiService} from '../../services/api.service';
import {ActivatedRoute, Router} from '@angular/router';
import {Logiciel} from '../../models/logiciel.model';
import {Editeur} from '../../models/editeur.model';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent implements OnInit {

  object: string;
  currentObject: string;
  currentLogiciel = null;
  message = '';
  form: FormGroup;
  list: Array<any> = [];



  constructor(
    private apiService: ApiService,
    private route: ActivatedRoute,
    private router: Router,
    private fb: FormBuilder) { }

  ngOnInit(): void {
    this.object = this.router.url.toString().split('/')[1];
    this.apiService.setBaseUrl(this.object);
    this.message = '';
    if (this.route.snapshot.paramMap.get('id') !== 'nouveau') {
      this.getLogiciel(this.route.snapshot.paramMap.get('id'));
    }
    else {
      this.initForm();
    }
  }


  getLogiciel(id): void {
    this.apiService.get(id)
      .subscribe(
        data => {
          this.currentLogiciel = data;
          this.form = new FormGroup({});
          for (const key in this.currentLogiciel) {
            if (this.currentLogiciel.hasOwnProperty(key) && key !== 'id') {
              const control: FormControl = new FormControl(this.currentLogiciel[key], Validators.required);
              this.form.addControl(key, control); // instead of this.obj[key]
              this.list.push(key);
            }
          }
        },
        error => {
          console.log(error);
        });
  }

  initForm(): void {
    this.initObject();
    this.form = new FormGroup({});
    for (const key in this.currentLogiciel) {
        console.log(key);
        const control: FormControl = new FormControl(this.currentLogiciel[key], Validators.required);
        this.form.addControl(key, control); // instead of this.obj[key]
        this.list.push(key);
    }
  }

  initObject(): void {
    switch (this.object) {
      case 'logiciels':
        this.currentLogiciel = new Logiciel(0, '', 0, 0, '', '', null, null);
        break;
      case 'editeurs':
        this.currentLogiciel = new Editeur(0, '');
        break;
    }
  }

  submit(): void {
    if (this.route.snapshot.paramMap.get('id') !== 'nouveau') {
      this.updateLogiciel();
    }
    else{
      this.saveLogiciel();
    }
  }

  updateLogiciel(): void {
    this.apiService.update(this.currentLogiciel.id, this.form.value)
      .subscribe(
        response => {
          console.log(response);
          this.message = 'The logicel was updated successfully!';
        },
        error => {
          console.log(error);
        });
  }

  saveLogiciel(): void {
    this.apiService.create(this.form.value)
      .subscribe(
        response => {
          console.log(response);
          console.log(this.form.value);
        },
        error => {
          console.log(error);
        });
  }

  deleteLogiciel(): void {
    this.apiService.delete(this.currentLogiciel.id)
      .subscribe(
        response => {
          console.log(response);
          this.router.navigate(['/logiciels']);
        },
        error => {
          console.log(error);
        });
  }


}
