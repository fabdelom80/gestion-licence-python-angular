import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';

import { AppComponent } from './app.component';
import {AppRoutingModule} from './app-routing.module';
import { EditeurDetailsComponent } from './components/editeur/editeur-details/editeur-details.component';
import { EditeursListComponent } from './components/editeur/editeurs-list/editeurs-list.component';
import { AddDemandeComponent } from './components/demande/add-demande/add-demande.component';
import { DemandeDetailsComponent } from './components/demande/demande-details/demande-details.component';
import { DemandesListComponent } from './components/demande/demandes-list/demandes-list.component';
import { LogicielDetailsComponent } from './components/logiciel/logiciel-details/logiciel-details.component';
import { LogicielsListComponent } from './components/logiciel/logiciels-list/logiciels-list.component';
import { MetriqueDetailsComponent } from './components/metrique/metrique-details/metrique-details.component';
import { MetriquesListComponent } from './components/metrique/metriques-list/metriques-list.component';
import { ServeurDetailsComponent } from './components/serveur/serveur-details/serveur-details.component';
import { ServeursListComponent } from './components/serveur/serveurs-list/serveurs-list.component';
import { TarifDetailsComponent } from './components/tarif/tarif-details/tarif-details.component';
import { TarifsListComponent } from './components/tarif/tarifs-list/tarifs-list.component';
import { UtilisateurDetailsComponent } from './components/utilisateur/utilisateur-details/utilisateur-details.component';
import { UtilisateursListComponent } from './components/utilisateur/utilisateurs-list/utilisateurs-list.component';
import { DsiDetailsComponent } from './components/dsi/dsi-details/dsi-details.component';
import { DsisListComponent } from './components/dsi/dsis-list/dsis-list.component';
import { AccueilComponent } from './components/accueil/accueil.component';
import { FormComponent } from './components/form/form.component';
import { ListeDeroulanteMultiComponent } from './components/liste-deroulante-multi/liste-deroulante-multi.component';
import {DatePipe} from '@angular/common';
import { NavbarComponent } from './components/navbar/navbar.component';
import { TopbarComponent } from './components/topbar/topbar.component';
import { StepperComponent } from './components/stepper/stepper.component';
import { TabComponent } from './components/tab/tab.component';


@NgModule({
  declarations: [
    AppComponent,
    EditeurDetailsComponent,
    EditeursListComponent,
    AddDemandeComponent,
    DemandeDetailsComponent,
    DemandesListComponent,
    LogicielDetailsComponent,
    LogicielsListComponent,
    MetriqueDetailsComponent,
    MetriquesListComponent,
    ServeurDetailsComponent,
    ServeursListComponent,
    TarifDetailsComponent,
    TarifsListComponent,
    UtilisateurDetailsComponent,
    UtilisateursListComponent,
    DsiDetailsComponent,
    DsisListComponent,
    AccueilComponent,
    FormComponent,
    ListeDeroulanteMultiComponent,
    NavbarComponent,
    TopbarComponent,
    StepperComponent,
    TabComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule
  ],
  providers: [DatePipe,],
  bootstrap: [AppComponent]
})
export class AppModule { }
