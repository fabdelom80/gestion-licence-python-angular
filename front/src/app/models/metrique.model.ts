export class Metrique {
  private id: number;
  private type: string;


  constructor(id: number, type: string) {
    this.id = id;
    this.type = type;
  }

  // doit être similaire à la variable colonnes de "emtriques-list.component.ts"
  public getTabObject(): Array<any> {
    const colonnes: Array<any> = [];
    colonnes.push(this.type);
    return colonnes;
  }

  public getId(): number {
    return this.id;
  }

  public getType(): string {
    return this.type;
  }

}
