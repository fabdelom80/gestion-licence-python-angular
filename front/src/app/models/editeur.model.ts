
export class Editeur {
  private id: number;
  private nom: string;


  constructor(id: number, nom: string) {
    this.id = id;
    this.nom = nom;
  }


  public getId(): number {
    return this.id;
  }

  public setId(value: number): void {
    this.id = value;
  }

  public getNom(): string {
    return this.nom;
  }

  public setNom(value: string): void {
    this.nom = value;
  }

  // doit être similaire à la variable colonnes de "editeurs-list.component.ts"
  public getTabObject(): Array<any> {
    const colonnes: Array<any> = [];
    colonnes.push(this.nom);
    return colonnes;
  }
}

