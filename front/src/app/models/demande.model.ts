import {Dsi} from './dsi.model';
import {Utilisateur} from './utilisateur.model';

export class Demande {
  private id: number;
  private statut: number;
  private dateCreation: string;
  private dateCloture: string;
  private dateSouhaitee: string;
  private projet: string;
  private clientDDSI: string;
  private mailProposition: string;
  private droitUsageLogiciel: string;
  private type: number;
  private quantite: number;
  private coreFactor: number;
  private information: string;
  private utilisateur: number;
  private dsi: number;
  private logiciels: Array<any> = [];
  private serveurs: Array<any> = [];


  constructor(id: number, statut: number, dateCreation: string, dateCloture: string, dateSouhaitee: string, projet: string, clientDDSI: string, mailProposition: string, droitUsageLogiciel: string, type: number, quantite: number, coreFactor: number, information: string, utilisateur: number, dsi: number) {
    this.id = id;
    this.statut = statut;
    this.dateCreation = dateCreation;
    this.dateCloture = dateCloture;
    this.dateSouhaitee = dateSouhaitee;
    this.projet = projet;
    this.clientDDSI = clientDDSI;
    this.mailProposition = mailProposition;
    this.droitUsageLogiciel = droitUsageLogiciel;
    this.type = type;
    this.quantite = quantite;
    this.coreFactor = coreFactor;
    this.information = information;
    this.utilisateur = utilisateur;
    this.dsi = dsi;
  }


  // doit être similaire à la variable colonnes de "demandes-list.component.ts"
  public getTabObject(): Array<any> {
    const colonnes: Array<any> = [];
    colonnes.push(this.utilisateur);
    colonnes.push(this.projet);
    colonnes.push(this.dsi);
    colonnes.push(this.dateSouhaitee);
    colonnes.push(this.dateCloture);
    return colonnes;
  }

  public addLogiciels(logiciel): void {
    this.logiciels = logiciel;
  }

  public getLogiciels(): Array<any> {
    return this.logiciels;
  }

  public addServeurs(serveur): void {
    this.serveurs = serveur;
  }

  public getServeurs(): Array<any> {
    return this.serveurs;
  }

  public getId(): number {
    return this.id;
  }
}
