export class Serveur {
  private id: number;
  private nom: string;
  private physique: boolean;
  private typeCpu: string;
  private modele: string;
  private cpu: number;
  private cores: number;
  private demande: number;


  constructor(id: number, nom: string, physique: boolean, typeCpu: string, modele: string, cpu: number, cores: number, demande: number) {
    this.id = id;
    this.nom = nom;
    this.physique = physique;
    this.typeCpu = typeCpu;
    this.modele = modele;
    this.cpu = cpu;
    this.cores = cores;
    this.demande = demande;
  }


}
