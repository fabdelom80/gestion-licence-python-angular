export class Dsi {
  private id: number;
  private nom: string;


  constructor(id: number, nom: string) {
    this.id = id;
    this.nom = nom;
  }


  // doit être similaire à la variable colonnes de "demandes-list.component.ts"
  public getTabObject(): Array<any> {
    const colonnes: Array<any> = [];
    colonnes.push(this.nom);
    return colonnes;
  }
  public getId(): number {
    return this.id;
  }
}
