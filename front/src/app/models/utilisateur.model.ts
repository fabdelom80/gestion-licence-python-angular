import {Dsi} from './dsi.model';

export class Utilisateur {
  private _id: number;
  private _cp: string;
  private _nom: string;
  private _prenom: string;
  private _mail: string;
  private _admin: boolean;
  private _dsi: Dsi;


  constructor(id: number, cp: string, nom: string, prenom: string, mail: string, admin: boolean, dsi: Dsi) {
    this._id = id;
    this._cp = cp;
    this._nom = nom;
    this._prenom = prenom;
    this._mail = mail;
    this._admin = admin;
    this._dsi = dsi;
  }

  get id(): number {
    return this._id;
  }

  set id(value: number) {
    this._id = value;
  }

  get cp(): string {
    return this._cp;
  }

  set cp(value: string) {
    this._cp = value;
  }

  get nom(): string {
    return this._nom;
  }

  set nom(value: string) {
    this._nom = value;
  }

  get prenom(): string {
    return this._prenom;
  }

  set prenom(value: string) {
    this._prenom = value;
  }

  get mail(): string {
    return this._mail;
  }

  set mail(value: string) {
    this._mail = value;
  }

  get admin(): boolean {
    return this._admin;
  }

  set admin(value: boolean) {
    this._admin = value;
  }

  get dsi(): Dsi {
    return this._dsi;
  }

  set dsi(value: Dsi) {
    this._dsi = value;
  }
}
