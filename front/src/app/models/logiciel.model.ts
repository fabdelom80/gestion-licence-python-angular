import {Metrique} from './metrique.model';
import {Editeur} from './editeur.model';

export class Logiciel {
  private id: number;
  private nom: string;
  private nbLicenceTotal: number;
  private nbLicenceRestante: number;
  private descriptif: string;
  private uniteDeMesure: string;
  private metrique: number;
  private editeur: number;


  constructor(id: number, nom: string, nbLicenceTotal: number, nbLicenceRestante: number, descriptif: string, uniteDeMesure: string, metrique: number, editeur: number) {
    this.id = id;
    this.nom = nom;
    this.nbLicenceTotal = nbLicenceTotal;
    this.nbLicenceRestante = nbLicenceRestante;
    this.descriptif = descriptif;
    this.uniteDeMesure = uniteDeMesure;
    this.metrique = metrique;
    this.editeur = editeur;
  }

  // doit être similaire à la variable colonnes de "logiciels-list.component.ts"
  public getTabObject(): Array<any> {
    const colonnes: Array<any> = [];
    colonnes.push(this.nom);
    colonnes.push(this.editeur);
    colonnes.push(this.metrique);
    colonnes.push(this.nbLicenceTotal);
    colonnes.push(this.nbLicenceRestante);
    return colonnes;
  }

  public getId(): number {
    return this.id;
  }

}
