import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EditeursListComponent } from './components/editeur/editeurs-list/editeurs-list.component';
import { EditeurDetailsComponent } from './components/editeur/editeur-details/editeur-details.component';
import {AccueilComponent} from './components/accueil/accueil.component';
import {LogicielsListComponent} from './components/logiciel/logiciels-list/logiciels-list.component';
import {LogicielDetailsComponent} from './components/logiciel/logiciel-details/logiciel-details.component';
import {AddDemandeComponent} from './components/demande/add-demande/add-demande.component';
import {FormComponent} from './components/form/form.component';
import {DemandesListComponent} from './components/demande/demandes-list/demandes-list.component';
import {DsisListComponent} from './components/dsi/dsis-list/dsis-list.component';
import {MetriquesListComponent} from './components/metrique/metriques-list/metriques-list.component';
import {DsiDetailsComponent} from './components/dsi/dsi-details/dsi-details.component';
import {MetriqueDetailsComponent} from './components/metrique/metrique-details/metrique-details.component';

const routes: Routes = [
  { path: '', redirectTo: 'index', pathMatch: 'full' },
  { path: 'index', component: AccueilComponent, pathMatch: 'full' },
  { path: 'editeurs', component: EditeursListComponent },
  { path: 'editeurs/:id', component: EditeurDetailsComponent },
  { path: 'logiciels', component: LogicielsListComponent },
  { path: 'logiciels/:id', component: LogicielDetailsComponent },
  { path: 'demandes', component: DemandesListComponent },
  { path: 'demandes/nouveau', component: AddDemandeComponent },
  { path: 'demandes/:id', component: FormComponent },
  { path: 'dsis', component: DsisListComponent },
  { path: 'dsis/:id', component: DsiDetailsComponent },
  { path: 'metriques', component: MetriquesListComponent },
  { path: 'metriques/:id', component: MetriqueDetailsComponent },
  { path: 'admin', component: AccueilComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
