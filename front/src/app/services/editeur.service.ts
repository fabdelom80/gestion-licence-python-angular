import {Injectable, OnInit} from '@angular/core';
import {ApiService} from './api.service';
import {Observable, Subject} from 'rxjs';
import {Editeur} from '../models/editeur.model';
import {logger} from 'codelyzer/util/logger';
import {FormControl, Validators} from '@angular/forms';

@Injectable({
  providedIn: 'root'
})
export class EditeurService{

  private mySubject: Subject<any> = new Subject();
  /*
  * mySubject will only be used inside this service
  * keep all your logic here, then check hello.component
  * to see how you can subscribe, easy peezy :)
  */
  public latestData: Observable<any> = this.mySubject.asObservable();
  private editeurs: Array<Editeur>;


  constructor(private apiService: ApiService) {
    this.apiService.setBaseUrl('editeurs');
  }


  getAll(): void {
   this.apiService.getAll().subscribe(
      data => {
        for (const i in data) {
          this.editeurs.push(new Editeur(data[i].id, data[i].nom))
        }
        console.log('je retourne ça ' + this.editeurs);
        this.mySubject.next(5);
      },
      error => {
        console.log(error);
        return null;
      });
  }

  get(id): Observable<any> {
    return this.apiService.get(id);
  }

  create(data): Observable<any> {
    return this.apiService.create(data);
  }

  update(id, data): Observable<any> {
    return this.apiService.update(id, data);
  }

  delete(id): Observable<any> {
    return this.apiService.delete(id);
  }

  deleteAll(): Observable<any> {
    return this.apiService.deleteAll();
  }

  findByNom(nom): Observable<any> {
    return this.apiService.findByNom(nom);
  }
}
