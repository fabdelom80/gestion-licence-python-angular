from django.db import models

class Editeur(models.Model):
    nom = models.CharField(max_length=400)
    def __str__(self):
        return self.nom


class Metrique(models.Model):
    type = models.CharField(max_length=400)
    def __str__(self):
        return self.type

class Logiciel(models.Model):
    nom = models.CharField(max_length=400)
    nbLicenceTotal = models.IntegerField()
    nbLicenceRestante = models.IntegerField()
    descriptif = models.CharField(max_length=400)
    uniteDeMesure = models.IntegerField()
    metrique = models.ForeignKey(Metrique, on_delete=models.CASCADE)
    editeur = models.ForeignKey(Editeur, on_delete=models.CASCADE)
    def __str__(self):
        return self.nom

class Tarif(models.Model):
    acquisition = models.IntegerField()
    descriptifAcquisition = models.CharField(max_length=400)
    majSupport = models.IntegerField()
    validite = models.DateField()
    perimetre = models.CharField(max_length=400)
    commentaire = models.CharField(max_length=400)
    logiciel = models.ForeignKey(Logiciel, on_delete=models.CASCADE)
    def __str__(self):
        return self.acquisition


class Dsi(models.Model):
    nom = models.CharField(max_length=400)
    def __str__(self):
        return self.nom


class Utilisateur(models.Model):
    cp = models.CharField(max_length=400)
    nom = models.CharField(max_length=400)
    prenom = models.CharField(max_length=400)
    mail = models.CharField(max_length=400)
    admin = models.BooleanField(default=False)
    dsi = models.ForeignKey(Dsi, on_delete=models.CASCADE)
    def __str__(self):
        return str(self.cp) + " " + self.nom + " " + self.prenom

class Demande(models.Model):
    statut = models.IntegerField()
    dateCreation = models.DateField()
    dateCloture = models.DateField(null=True)
    dateSouhaitee = models.DateField()
    projet = models.CharField(max_length=400)
    mailProposition = models.CharField(max_length=400)
    clientDDSI = models.CharField(max_length=400)
    droitUsageLogiciel = models.CharField(max_length=400)
    type = models.IntegerField()
    quantite = models.IntegerField()
    coreFactor = models.FloatField()
    commentaire = models.CharField(max_length=400, null=True)
    information = models.CharField(max_length=400, null=True)
    utilisateur = models.ForeignKey(Utilisateur, on_delete=models.CASCADE)
    dsi = models.ForeignKey(Dsi, on_delete=models.CASCADE)
    def __str__(self):
        return self.logiciel.nom


class Serveur(models.Model):
    nom = models.CharField(max_length=400)
    physique = models.BooleanField()
    typeCpu = models.CharField(max_length=400)
    modele = models.CharField(max_length=400)
    cpu = models.IntegerField(null=True)
    cores = models.IntegerField(null=True)
    demande = models.ForeignKey(Demande, on_delete=models.CASCADE)

    def __str__(self):
        return self.nom

class LogicielDemande(models.Model):
    logiciel = models.ForeignKey(Logiciel, on_delete=models.CASCADE)
    demande = models.ForeignKey(Demande, on_delete=models.CASCADE)
    def __str__(self):
        return self.logiciel + " " + self.demande
