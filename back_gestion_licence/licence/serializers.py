from rest_framework import serializers 
from licence.models import Editeur, Metrique, Logiciel, Tarif, Dsi, Utilisateur, Serveur, Demande, LogicielDemande


class UtilisateurSerializer(serializers.ModelSerializer):
 
    class Meta:
        model = Utilisateur
        fields = '__all__'


class EditeurSerializer(serializers.ModelSerializer):
    class Meta:
        model = Editeur
        fields = '__all__'


class MetriqueSerializer(serializers.ModelSerializer):
    class Meta:
        model = Metrique
        fields = '__all__'


class LogicielSerializer(serializers.ModelSerializer):
    class Meta:
        model = Logiciel
        fields = '__all__'

class GetLogicielSerializer(serializers.ModelSerializer):
    class Meta:
        model = Logiciel
        fields = '__all__'
        depth = 1 #pour afficher l'objet au complet et pas seulement son ID



class TarifSerializer(serializers.ModelSerializer):
    class Meta:
        model = Tarif
        fields = '__all__'


class DsiSerializer(serializers.ModelSerializer):
    class Meta:
        model = Dsi
        fields = '__all__'

class ServeurSerializer(serializers.ModelSerializer):
    class Meta:
        model = Serveur
        fields = '__all__'

class GetServeurSerializer(serializers.ModelSerializer):
    class Meta:
        model = Serveur
        fields = '__all__'
        depth = 1


class DemandeSerializer(serializers.ModelSerializer):
    class Meta:
        model = Demande
        fields = '__all__'

class GetDemandeSerializer(serializers.ModelSerializer):
    class Meta:
        model = Demande
        fields = '__all__'
        depth = 1  # pour afficher l'objet au complet et pas seulement son ID

class LogicielDemandeSerializer(serializers.ModelSerializer):
    class Meta:
        model = LogicielDemande
        fields = '__all__'

class GetLogicielDemandeSerializer(serializers.ModelSerializer):
    class Meta:
        model = LogicielDemande
        fields = '__all__'
        depth = 1 #pour afficher l'objet au complet et pas seulement son ID

