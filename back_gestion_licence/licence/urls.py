from django.conf.urls import url
from django.urls import path
from licence import views
 
urlpatterns = [ 
    url(r'^api/editeurs$', views.editeur_list),
    url(r'^api/editeurs/(?P<pk>[0-9]+)$', views.editeur_detail),
    url(r'^api/metriques$', views.metrique_list),
    url(r'^api/metriques/(?P<pk>[0-9]+)$', views.metrique_detail),
    url(r'^api/logiciels$', views.logiciel_list),
    url(r'^api/logiciels/(?P<pk>[0-9]+)$', views.logiciel_detail),
    url(r'^api/logiciels/editeur/(?P<pk>[0-9]+)$', views.logiciel_editeur_list),
    url(r'^api/dsi$', views.dsi_list),
    url(r'^api/dsi/(?P<pk>[0-9]+)$', views.dsi_detail),
    url(r'^api/utilisateurs$', views.utilisateur_list),
    url(r'^api/utilisateurs/(?P<pk>[0-9]+)$', views.utilisateur_detail),
    url(r'^api/serveurs$', views.serveur_list),
    url(r'^api/serveurs/demande/(?P<pk>[0-9]+)$', views.serveur_list_idDemande),
    url(r'^api/serveurs/(?P<pk>[0-9]+)$', views.serveur_detail),
    url(r'^api/demandes$', views.demande_list),
    url(r'^api/demandes/(?P<pk>[0-9]+)$', views.demande_detail),
    url(r'^api/tarifs$', views.tarif_list),
    url(r'^api/tarifs/(?P<pk>[0-9]+)$', views.tarif_detail),
    url(r'^api/logicielDemande$', views.logicielDemande_list),
    url(r'^api/logicielDemande/demande/(?P<pk>[0-9]+)$', views.logicielDemande_list_idDemande),
    url(r'^api/logicielDemande/(?P<pk>[0-9]+)$', views.logicielDemande_detail),

]
