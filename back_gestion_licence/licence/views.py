

from django.http.response import JsonResponse
from rest_framework.parsers import JSONParser
from rest_framework import status

from licence.models import Editeur, Metrique, Logiciel, Tarif, Dsi, Utilisateur, Serveur, Demande, LogicielDemande
from licence.serializers import EditeurSerializer, MetriqueSerializer, LogicielSerializer, TarifSerializer, \
    DemandeSerializer, UtilisateurSerializer, ServeurSerializer, DsiSerializer, LogicielDemandeSerializer, \
    GetDemandeSerializer, GetLogicielDemandeSerializer, \
    GetLogicielSerializer, GetServeurSerializer
from rest_framework.decorators import api_view


####### EDITEUR ########

@api_view(['GET', 'POST', 'DELETE'])
def editeur_list(request):
    if request.method == 'GET':
        objects = Editeur.objects.all()

        title = request.GET.get('nom', None)
        if title is not None:
            objects = objects.filter(nom__icontains=title)

        objects_serializer = EditeurSerializer(objects, many=True)
        return JsonResponse(objects_serializer.data, safe=False)
        # 'safe=False' for objects serialization

    elif request.method == 'POST':
        object_data = JSONParser().parse(request)
        object_serializer = EditeurSerializer(data=object_data)
        if object_serializer.is_valid():
            object_serializer.save()
            return JsonResponse(object_serializer.data, status=status.HTTP_201_CREATED)
        return JsonResponse(object_serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    elif request.method == 'DELETE':
        count = Editeur.objects.all().delete()
        return JsonResponse({'message': '{} Editeurs were deleted successfully!'.format(count[0])},
                            status=status.HTTP_204_NO_CONTENT)


@api_view(['GET', 'PUT', 'DELETE'])
def editeur_detail(request, pk):
    try:
        object = Editeur.objects.get(pk=pk)
    except Editeur.DoesNotExist:
        return JsonResponse({'message': 'The editeur does not exist'}, status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        object_serializer = EditeurSerializer(object)
        return JsonResponse(object_serializer.data)

    elif request.method == 'PUT':
        object_data = JSONParser().parse(request)
        object_serializer = EditeurSerializer(object, data=object_data)
        if object_serializer.is_valid():
            object_serializer.save()
            return JsonResponse(object_serializer.data)
        return JsonResponse(object_serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    elif request.method == 'DELETE':
        object.delete()
        return JsonResponse({'message': 'Editeur was deleted successfully!'}, status=status.HTTP_204_NO_CONTENT)


    ####### METRIQUE ########

@api_view(['GET', 'POST', 'DELETE'])
def metrique_list(request):
    if request.method == 'GET':
        objects = Metrique.objects.all()

        title = request.GET.get('type', None)
        if title is not None:
            objects = objects.filter(nom__icontains=title)

        objects_serializer = MetriqueSerializer(objects, many=True)
        return JsonResponse(objects_serializer.data, safe=False)
        # 'safe=False' for objects serialization

    elif request.method == 'POST':
        object_data = JSONParser().parse(request)
        object_serializer = MetriqueSerializer(data=object_data)
        if object_serializer.is_valid():
            object_serializer.save()
            return JsonResponse(object_serializer.data, status=status.HTTP_201_CREATED)
        return JsonResponse(object_serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    elif request.method == 'DELETE':
        count = Metrique.objects.all().delete()
        return JsonResponse({'message': '{} Metriques were deleted successfully!'.format(count[0])},
                            status=status.HTTP_204_NO_CONTENT)


@api_view(['GET', 'PUT', 'DELETE'])
def metrique_detail(request, pk):
    try:
        object = Metrique.objects.get(pk=pk)
    except Editeur.DoesNotExist:
        return JsonResponse({'message': 'The metrique does not exist'}, status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        object_serializer = MetriqueSerializer(object)
        return JsonResponse(object_serializer.data)

    elif request.method == 'PUT':
        object_data = JSONParser().parse(request)
        object_serializer = MetriqueSerializer(object, data=object_data)
        if object_serializer.is_valid():
            object_serializer.save()
            return JsonResponse(object_serializer.data)
        return JsonResponse(object_serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    elif request.method == 'DELETE':
        object.delete()
        return JsonResponse({'message': 'metrique was deleted successfully!'}, status=status.HTTP_204_NO_CONTENT)




    ####### LOGICIEL ########

@api_view(['GET', 'POST', 'DELETE'])
def logiciel_list(request):
    if request.method == 'GET':
        objects = Logiciel.objects.all()

        title = request.GET.get('nom', None)
        if title is not None:
            objects = objects.filter(nom__icontains=title)

        objects_serializer = GetLogicielSerializer(objects, many=True)
        return JsonResponse(objects_serializer.data, safe=False)
        # 'safe=False' for objects serialization

    elif request.method == 'POST':
        object_data = JSONParser().parse(request)
        object_serializer = LogicielSerializer(data=object_data)
        if object_serializer.is_valid():
            object_serializer.save()
            return JsonResponse(object_serializer.data, status=status.HTTP_201_CREATED)
        return JsonResponse(object_serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    elif request.method == 'DELETE':
        count = Logiciel.objects.all().delete()
        return JsonResponse({'message': '{} Logiciels were deleted successfully!'.format(count[0])},
                            status=status.HTTP_204_NO_CONTENT)


@api_view(['GET', 'PUT', 'DELETE'])
def logiciel_detail(request, pk):
    try:
        object = Logiciel.objects.get(pk=pk)
    except Logiciel.DoesNotExist:
        return JsonResponse({'message': 'The logiciel does not exist'}, status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        object_serializer = GetLogicielSerializer(object)
        return JsonResponse(object_serializer.data)

    elif request.method == 'PUT':
        object_data = JSONParser().parse(request)
        object_serializer = LogicielSerializer(object, data=object_data)
        if object_serializer.is_valid():
            object_serializer.save()
            return JsonResponse(object_serializer.data)
        return JsonResponse(object_serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    elif request.method == 'DELETE':
        object.delete()
        return JsonResponse({'message': 'logiciel was deleted successfully!'}, status=status.HTTP_204_NO_CONTENT)

@api_view(['GET'])
def logiciel_editeur_list(request, pk):
    try:
        objects = Logiciel.objects.filter(editeur=pk)
    except Logiciel.DoesNotExist:
        return JsonResponse({'message': 'The logiciel does not exist'}, status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        objects_serializer = LogicielSerializer(objects, many=True)
        return JsonResponse(objects_serializer.data, safe=False)



   ####### Tarif ########

@api_view(['GET', 'POST', 'DELETE'])
def tarif_list(request):
    if request.method == 'GET':
        objects = Tarif.objects.all()
        objects_serializer = TarifSerializer(objects, many=True)
        return JsonResponse(objects_serializer.data, safe=False)
        # 'safe=False' for objects serialization

    elif request.method == 'POST':
        object_data = JSONParser().parse(request)
        object_serializer = TarifSerializer(data=object_data)
        if object_serializer.is_valid():
            object_serializer.save()
            return JsonResponse(object_serializer.data, status=status.HTTP_201_CREATED)
        return JsonResponse(object_serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    elif request.method == 'DELETE':
        count = Tarif.objects.all().delete()
        return JsonResponse({'message': '{} Tarifs were deleted successfully!'.format(count[0])},
                            status=status.HTTP_204_NO_CONTENT)


@api_view(['GET', 'PUT', 'DELETE'])
def tarif_detail(request, pk):
    try:
        object = Tarif.objects.get(pk=pk)
    except Tarif.DoesNotExist:
        return JsonResponse({'message': 'The tarif does not exist'}, status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        object_serializer = TarifSerializer(object)
        return JsonResponse(object_serializer.data)

    elif request.method == 'PUT':
        object_data = JSONParser().parse(request)
        object_serializer = TarifSerializer(object, data=object_data)
        if object_serializer.is_valid():
            object_serializer.save()
            return JsonResponse(object_serializer.data)
        return JsonResponse(object_serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    elif request.method == 'DELETE':
        object.delete()
        return JsonResponse({'message': 'tarif was deleted successfully!'}, status=status.HTTP_204_NO_CONTENT)



   ####### DSI ########

@api_view(['GET', 'POST', 'DELETE'])
def dsi_list(request):
    if request.method == 'GET':
        objects = Dsi.objects.all()
        objects_serializer = DsiSerializer(objects, many=True)
        return JsonResponse(objects_serializer.data, safe=False)
        # 'safe=False' for objects serialization

    elif request.method == 'POST':
        object_data = JSONParser().parse(request)
        object_serializer = DsiSerializer(data=object_data)
        if object_serializer.is_valid():
            object_serializer.save()
            return JsonResponse(object_serializer.data, status=status.HTTP_201_CREATED)
        return JsonResponse(object_serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    elif request.method == 'DELETE':
        count = Dsi.objects.all().delete()
        return JsonResponse({'message': '{} Dsi were deleted successfully!'.format(count[0])},
                            status=status.HTTP_204_NO_CONTENT)


@api_view(['GET', 'PUT', 'DELETE'])
def dsi_detail(request, pk):
    try:
        object = Dsi.objects.get(pk=pk)
    except Dsi.DoesNotExist:
        return JsonResponse({'message': 'The dsi does not exist'}, status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        object_serializer = DsiSerializer(object)
        return JsonResponse(object_serializer.data)

    elif request.method == 'PUT':
        object_data = JSONParser().parse(request)
        object_serializer = DsiSerializer(object, data=object_data)
        if object_serializer.is_valid():
            object_serializer.save()
            return JsonResponse(object_serializer.data)
        return JsonResponse(object_serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    elif request.method == 'DELETE':
        object.delete()
        return JsonResponse({'message': 'dsi was deleted successfully!'}, status=status.HTTP_204_NO_CONTENT)

####### Utilisateur ########

@api_view(['GET', 'POST', 'DELETE'])
def utilisateur_list(request):
    if request.method == 'GET':
        objects = Utilisateur.objects.all()
        objects_serializer = UtilisateurSerializer(objects, many=True)
        return JsonResponse(objects_serializer.data, safe=False)
        # 'safe=False' for objects serialization

    elif request.method == 'POST':
        object_data = JSONParser().parse(request)
        object_serializer = UtilisateurSerializer(data=object_data)
        if object_serializer.is_valid():
            object_serializer.save()
            return JsonResponse(object_serializer.data, status=status.HTTP_201_CREATED)
        return JsonResponse(object_serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    elif request.method == 'DELETE':
        count = Utilisateur.objects.all().delete()
        return JsonResponse({'message': '{} Utilisateurs were deleted successfully!'.format(count[0])},
                            status=status.HTTP_204_NO_CONTENT)

@api_view(['GET', 'PUT', 'DELETE'])
def utilisateur_detail(request, pk):
    try:
        object = Utilisateur.objects.get(pk=pk)
    except Utilisateur.DoesNotExist:
        return JsonResponse({'message': 'The utilisateur does not exist'}, status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        object_serializer = UtilisateurSerializer(object)
        return JsonResponse(object_serializer.data)

    elif request.method == 'PUT':
        object_data = JSONParser().parse(request)
        object_serializer = UtilisateurSerializer(object, data=object_data)
        if object_serializer.is_valid():
            object_serializer.save()
            return JsonResponse(object_serializer.data)
        return JsonResponse(object_serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    elif request.method == 'DELETE':
        object.delete()
        return JsonResponse({'message': 'utilisateur was deleted successfully!'}, status=status.HTTP_204_NO_CONTENT)


####### Serveur ########

@api_view(['GET', 'POST', 'DELETE'])
def serveur_list(request):
    if request.method == 'GET':
        objects = Serveur.objects.all()
        objects_serializer = GetServeurSerializer(objects, many=True)
        return JsonResponse(objects_serializer.data, safe=False)
        # 'safe=False' for objects serialization

    elif request.method == 'POST':
        list_object_serializer = []
        serveurs = JSONParser().parse(request)
        for serveur in serveurs:
            object_serializer = ServeurSerializer(data=serveur)
            if object_serializer.is_valid():
                object_serializer.save()
                list_object_serializer.append(object_serializer.data)
            else:
                return JsonResponse(object_serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        return JsonResponse(list_object_serializer, status=status.HTTP_201_CREATED, safe=False)


    elif request.method == 'DELETE':
        count = Serveur.objects.all().delete()
        return JsonResponse({'message': '{} Serveurs were deleted successfully!'.format(count[0])},
                            status=status.HTTP_204_NO_CONTENT)

@api_view(['GET', 'PUT', 'DELETE'])
def serveur_detail(request, pk):
    try:
        object = Serveur.objects.get(pk=pk)
    except Serveur.DoesNotExist:
        return JsonResponse({'message': 'The serveur does not exist'}, status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        object_serializer = GetServeurSerializer(object)
        return JsonResponse(object_serializer.data)

    elif request.method == 'PUT':
        object_data = JSONParser().parse(request)
        object_serializer = ServeurSerializer(object, data=object_data)
        if object_serializer.is_valid():
            object_serializer.save()
            return JsonResponse(object_serializer.data)
        return JsonResponse(object_serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    elif request.method == 'DELETE':
        object.delete()
        return JsonResponse({'message': 'serveur was deleted successfully!'}, status=status.HTTP_204_NO_CONTENT)

@api_view(['GET', 'PUT', 'DELETE'])
def serveur_list_idDemande(request, pk):
    if request.method == 'GET':
        objects = Serveur.objects.filter(demande_id=pk)
        objects_serializer = GetServeurSerializer(objects, many=True)
        return JsonResponse(objects_serializer.data, safe=False)
        # 'safe=False' for objects serialization

    elif request.method == 'DELETE':
        count = Serveur.objects.filter(demande_id=pk).delete()
        return JsonResponse({'message': '{} Serveurs were deleted successfully!'.format(count[0])},
                            status=status.HTTP_204_NO_CONTENT)



####### Demande ########

@api_view(['GET', 'POST', 'DELETE'])
def demande_list(request):
    if request.method == 'GET':
        objects = Demande.objects.all()
        objects_serializer = GetDemandeSerializer(objects, many=True)
        return JsonResponse(objects_serializer.data, safe=False)
        # 'safe=False' for objects serialization

    elif request.method == 'POST':
        object_data = JSONParser().parse(request)
        object_serializer = DemandeSerializer(data=object_data)
        if object_serializer.is_valid():
            object_serializer.save()
            return JsonResponse(object_serializer.data, status=status.HTTP_201_CREATED)
        return JsonResponse(object_serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    elif request.method == 'DELETE':
        count = Demande.objects.all().delete()
        return JsonResponse({'message': '{} Demandes were deleted successfully!'.format(count[0])},
                            status=status.HTTP_204_NO_CONTENT)

@api_view(['GET', 'PUT', 'DELETE'])
def demande_detail(request, pk):
    try:
        object = Demande.objects.get(pk=pk)
    except Demande.DoesNotExist:
        return JsonResponse({'message': 'The demande does not exist'}, status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        object_serializer = GetDemandeSerializer(object)
        return JsonResponse(object_serializer.data)

    elif request.method == 'PUT':
        object_data = JSONParser().parse(request)
        object_serializer = DemandeSerializer(object, data=object_data)
        if object_serializer.is_valid():
            object_serializer.save()
            return JsonResponse(object_serializer.data)
        return JsonResponse(object_serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    elif request.method == 'DELETE':
        object.delete()
        return JsonResponse({'message': 'demande was deleted successfully!'}, status=status.HTTP_204_NO_CONTENT)

####### LogicielDemande ########

@api_view(['GET', 'POST', 'DELETE'])
def logicielDemande_list(request):
    if request.method == 'GET':
        objects = LogicielDemande.objects.all()
        objects_serializer = GetLogicielDemandeSerializer(objects, many=True)
        return JsonResponse(objects_serializer.data, safe=False)
        # 'safe=False' for objects serialization

    elif request.method == 'POST':
        list_object_serializer = []
        logiciels = JSONParser().parse(request);
        for logiciel in logiciels:
            object_serializer = LogicielDemandeSerializer(data=logiciel)
            if object_serializer.is_valid():
                object_serializer.save()
                list_object_serializer.append(object_serializer.data)
            else:
                return JsonResponse(object_serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        return JsonResponse(list_object_serializer, status=status.HTTP_201_CREATED, safe=False)

@api_view(['GET', 'PUT', 'DELETE'])
def logicielDemande_detail(request, pk):
    try:
        object = LogicielDemande.objects.get(pk=pk)
    except LogicielDemande.DoesNotExist:
        return JsonResponse({'message': 'The logicielDemande does not exist'}, status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        object_serializer = GetLogicielDemandeSerializer(object)
        return JsonResponse(object_serializer.data)

    elif request.method == 'PUT':
        object_data = JSONParser().parse(request)
        object_serializer = LogicielDemandeSerializer(object, data=object_data)
        if object_serializer.is_valid():
            object_serializer.save()
            return JsonResponse(object_serializer.data)
        return JsonResponse(object_serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    elif request.method == 'DELETE':
        object.delete()
        return JsonResponse({'message': 'logicieldemande was deleted successfully!'}, status=status.HTTP_204_NO_CONTENT)



@api_view(['GET', 'PUT', 'DELETE'])
def logicielDemande_list_idDemande(request, pk):
    if request.method == 'GET':
        objects = LogicielDemande.objects.filter(demande_id=pk)
        objects_serializer = GetLogicielDemandeSerializer(objects, many=True)
        return JsonResponse(objects_serializer.data, safe=False)
        # 'safe=False' for objects serialization

    elif request.method == 'DELETE':
        count = LogicielDemande.objects.filter(demande_id=pk).delete()
        return JsonResponse({'message': '{} LogicielDemande were deleted successfully!'.format(count[0])},
                            status=status.HTTP_204_NO_CONTENT)



