# Generated by Django 3.0.7 on 2020-07-28 08:46

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('licence', '0005_auto_20200728_1045'),
    ]

    operations = [
        migrations.AlterField(
            model_name='demande',
            name='dateCloture',
            field=models.DateField(null=True),
        ),
    ]
