# Generated by Django 3.0.7 on 2020-07-28 09:31

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('licence', '0006_auto_20200728_1046'),
    ]

    operations = [
        migrations.AddField(
            model_name='demande',
            name='mailProposition',
            field=models.CharField(default='', max_length=400),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='demande',
            name='information',
            field=models.CharField(max_length=400, null=True),
        ),
    ]
