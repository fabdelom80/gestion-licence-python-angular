# Generated by Django 3.0.8 on 2020-07-24 10:03

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('licence', '0001_initial'),
    ]

    operations = [
        migrations.RenameField(
            model_name='utilisateur',
            old_name='entite',
            new_name='dsi',
        ),
        migrations.CreateModel(
            name='LogicielDemande',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('demande', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='licence.Demande')),
                ('logiciel', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='licence.Logiciel')),
            ],
        ),
    ]
